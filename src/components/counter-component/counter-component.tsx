import { Component, Host, h, State, Prop,Event, EventEmitter } from '@stencil/core';

@Component({
  tag: 'counter-component',
  styleUrl: 'counter-component.css',
  shadow: true,
})
export class CounterComponent {

  @Prop() btntext: string;
  @Prop() color: string;

  @State() counter = 0;

  @Event() didreset: EventEmitter

  increase() {
    this.counter++;
  }

  reset() {
    this.counter = 0
    this.didreset.emit(true)
  }

  render() {
    return (
      <Host>
        <button onClick={() => this.increase()}>{this.btntext}</button>
        <div>{this.counter}</div>
        <button onClick={() => this.reset()}>Reset</button>
        <slot name="help"></slot>
      </Host>
    );
  }

}
