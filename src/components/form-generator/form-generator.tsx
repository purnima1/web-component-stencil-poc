import { Component, Host, h, Prop, State, Watch, Event, EventEmitter } from '@stencil/core';

@Component({
  tag: 'form-generator',
  styleUrl: 'form-generator.css',
  shadow: true,
})
export class FormGenerator {

  @Prop() formdata: string;
  @Prop() buttonname: string;
  @State() formschema = [];
  @State() formvalues= [];
  @Event() sendformdata: EventEmitter

  divElement;

  getElementHere() {
    this.divElement  // this will refer to your <div> element
  }

  componentWillLoad() {
    this.parseOptions();
  }

  setformdata() {
    this.formschema.map((form, index) => {
      const ele = this.divElement.querySelector('#wc-value-'+index).value
      this.formvalues.push({field: form.title, value: ele})
      
    })

    this.sendformdata.emit({formfilleddata: this.formvalues})

  }

  @Watch('formdata')
  parseOptions() {
    if (this.formdata) {
      this.formschema = JSON.parse(this.formdata);
    }
  }

  render() {
    return (
      <Host>
       <div ref={(el) => this.divElement= el as HTMLElement}>
        <div class="wc-form-wrapper">
          {this.formschema.map((form, index) => {
           return (
            <div class={`wc-field-wrapper ${form.className}`}>
              <label class={`wc-label ${form.className}`}>{form.title}</label>
              <input id={`wc-value-`+ index} class={`wc-input ${form.className}`} type={form.type}/>
            </div>
          );
          })}
          <div>
            <button class="wc-action-button" onClick={() => this.setformdata()}>{this.buttonname}</button>
            {/* we can use slot to customize our button sections as we want */}
            {/* <slot></slot> */}
          </div>
        </div>
       </div>
      </Host>
    );
  }

}
