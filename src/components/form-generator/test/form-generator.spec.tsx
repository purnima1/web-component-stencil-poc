import { newSpecPage } from '@stencil/core/testing';
import { FormGenerator } from '../form-generator';

describe('form-generator', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [FormGenerator],
      html: `<form-generator></form-generator>`,
    });
    expect(page.root).toEqualHtml(`
      <form-generator>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </form-generator>
    `);
  });
});
