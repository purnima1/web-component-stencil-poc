import { newE2EPage } from '@stencil/core/testing';

describe('form-generator', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<form-generator></form-generator>');

    const element = await page.find('form-generator');
    expect(element).toHaveClass('hydrated');
  });
});
